﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPoolModif : MonoBehaviour {

    #region Inner classe
    [System.Serializable]
    public class SpawnConfig
    {

        /// <summary>
        /// scrool speed of the game
        /// </summary>
        [Range(0.5f, 2.5f)]
        public float ScrollSpeed;
        /// <summary>
        /// spawn postition x for the first columns
        /// </summary>
        public float StartX;
        /// <summary>
        /// min spawn pos x for the next columns
        /// </summary>
        public float SpawnMin;
        /// <summary>
        /// max spawn pos x for the next columns
        /// </summary>
        public float SpawnMax;
        /// <summary>
        /// min spawn pos y for the next columns
        /// </summary>
        public float HMin;
        /// <summary>
        /// max spawn pos y for the next columns
        /// </summary>
        public float HMax;
        /// <summary>
        /// min spacing between thetop and the bottom columns
        /// </summary>
        public float SpacingMin;

        /// <summary>
        /// max spacin between the top and the bottom columns
        /// </summary>
        public float SpacingMax;

        /// <summary>
        /// chance for a columns to have a top and bottom part
        /// </summary>
        [Range(0, 1)]
        public float PrctDouble = 0.5f;
    }
    #endregion

    #region Variable
    private GameObject[] columns;
    private int currentColumn = 0;
    public GameObject columnPrefab;
    public int columnPoolSize = 5;

    public SpawnConfig Config;
    #endregion


    #region Instantiate des colonnes

    void Start()
    {
        columns = new GameObject[columnPoolSize];
        for (int i = 0; i < columnPoolSize; i++)
        {
            //crée un objet colonne, stock une ref vers l'objet dans le tableau
            columns[i] = Instantiate(columnPrefab);
            //désactiver l'objet
            columns[i].SetActive(false);
        }
        SpawnColumn(Config, 0);
    }
    #endregion

    #region Spawn

    void SpawnColumn(SpawnConfig config, float lastColumnPositionX)
    {
        
        var nextColumn = currentColumn + 1 >= columnPoolSize ? 0 : currentColumn + 1;

        var column = columns[nextColumn];

        column.SetActive(true);

        Vector3 position = new Vector3(lastColumnPositionX, 0, 0);

        position.x += Random.Range(config.SpawnMin, config.SpawnMax);
        position.y += Random.Range(config.HMin, config.HMax);

        column.transform.position = position;

        bool IsDouble = Random.Range(0f, 1f) < config.PrctDouble;

        foreach (Transform child in column.transform)
        {
            child.gameObject.SetActive(true);
        }

        if (!IsDouble)
        {
            //l'enfant à faire disparaitre
            var childCount = column.transform.childCount;
            var hidePart = column.transform.GetChild(Random.Range(0, childCount));
            //cacher l'enfant
            hidePart.gameObject.SetActive(true);
        }
        //espacement entre les colonnes

        var spacing = Random.Range(config.SpacingMin, config.SpacingMax);
        column.transform.GetChild(0).localPosition = Vector3.up * spacing * 0.5f;
        column.transform.GetChild(1).localPosition = Vector3.down * spacing * 0.5f;

        currentColumn = nextColumn;

    }
    #endregion


    private void Update()
    {
        if(columns[currentColumn].transform.position.x <= 8)
        {
            SpawnColumn(Config, columns[currentColumn].transform.position.x);
            
        }
    }


}
